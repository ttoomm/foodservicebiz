package biz.foodservice.transaction;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

import com.google.appengine.api.datastore.Entity;





//@JsonTypeInfo(use=JsonTypeInfo.Id.CLASS, include=JsonTypeInfo.As.PROPERTY, property="$type")
public class Test
{
    @JsonProperty("n")
    public String name="afd";
    //@JsonIgnore
    //public boolean readOnly;
    @JsonProperty("t")
    public int type=99;
    
    @JsonIgnore
    public long ffff=87L;
    
    public int aa;
    
    //@JsonIgnore
    public Map<Integer, Integer> m;
    
    /*
    @JsonProperty("m")
    public Properties getM(){
    	Properties props=new Properties();
    	for(int i: this.m.keySet()){
    		props.put(i, this.m.get(i));
    	}
    	return props;
    }
    
    @JsonProperty("m")
    public void setM(Properties ps){
    	for(Object i: ps.keySet()){
    		Object v=ps.get(i);
    		this.m.put(Integer.parseInt((String) i), Integer.parseInt(v+""));
    	}
    }
    */
    
    
    public Test(){
    	this.m=new HashMap<Integer, Integer>();
    	this.m.put(44, 55);
    	this.m.put(66, 77);
    	this.aa=987;
    }
    
    private Map<Integer, Integer> fromString(String s) throws JsonParseException, JsonMappingException, IOException{
    	ObjectMapper om= new ObjectMapper();
    	Map<Integer, Integer> m=om.readValue(s, new TypeReference<Map<Integer, Integer>>(){});
    	return m;
    }
    private String fromMap(Map<Integer, Integer> m) throws JsonGenerationException, JsonMappingException, IOException{
    	ObjectMapper om= new ObjectMapper();
    	return om.writeValueAsString(m);
    }
    
    @JsonIgnore
    public Entity getEntity() throws JsonGenerationException, JsonMappingException, IOException{
    	Entity e=new Entity(Test.class.getCanonicalName());
    	e.setProperty("m", this.fromMap(this.m));
    	e.setProperty("aa", this.aa);
    	e.setProperty("ab", 98798);
    	return e;
    }

    
}

