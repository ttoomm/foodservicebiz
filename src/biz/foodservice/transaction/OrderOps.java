package biz.foodservice.transaction;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import biz.foodservice.model.CookingTask;
import biz.foodservice.model.DeliveryTask;
import biz.foodservice.model.Dish;
import biz.foodservice.model.Order;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.CompositeFilterOperator;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;



@Path("/transaction/order")
public class OrderOps   {



	private DatastoreService ds;
	
    
    private DatastoreService getService(){
    	if(this.ds==null){
    		this.ds=DatastoreServiceFactory.getDatastoreService();
    	}
    	return this.ds;
    }
    
    public void refreshOrder() throws EntityNotFoundException{
    	
    	DatastoreService ds=this.getService();
    	Filter f1=new FilterPredicate("deliveried", FilterOperator.EQUAL, false);
    	Filter f2=new FilterPredicate("handled", FilterOperator.EQUAL, true);
    	
    	Filter f=CompositeFilterOperator.and(f1, f2);
    	Query q=new Query(Order.KIND).setFilter(f);
    	PreparedQuery pq=ds.prepare(q);
    	nextOrder: for(Entity e: pq.asIterable()){
    		Order order=new Order(e);
    		for(long cookingTaskId: order.cookingTasks){
    			Key k=KeyFactory.createKey(CookingTask.KIND, cookingTaskId);
    			Entity ect=ds.get(k);
    			CookingTask ct=new CookingTask(ect);
    			if(ct.status!=CookingTask.SEND_TO_DELIVER)
    				continue nextOrder;
    		}

    		Filter fb=new FilterPredicate("orderId", FilterOperator.EQUAL, order.id);

    		Query qq=new Query(DeliveryTask.KIND).setFilter(fb);
    		List<DeliveryTask> dts=new ArrayList<DeliveryTask>();
    		PreparedQuery pq1=ds.prepare(qq);
    		for(Entity ea: pq1.asIterable()){
    			DeliveryTask dt=new DeliveryTask(ea);
    			if(dt.status!=DeliveryTask.status_finished)
    				continue nextOrder;
    			dt.status=dt.status_HandleByPayment;
    			dts.add(dt);
    		}
    		order.delivered=true;
    		List<Entity> el=new ArrayList<Entity>();
    		el.add(order.getEntity());
    		for(DeliveryTask dt: dts){
    			el.add(dt.getEntity());
    		}
    		TransactionOptions options = TransactionOptions.Builder.withXG(true);
    		Transaction transaction =ds.beginTransaction(options);
    		ds.put(el);
    		transaction.commit();
    	}
    	
    }
    
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/unhandled")
    public List<Order> getUnhandledOrder(){
    	DatastoreService ds=this.getService();
    	List<Order> orders=new ArrayList<Order>();
    	
    	Filter f=new FilterPredicate("handled" , FilterOperator.EQUAL, false);
    	Query q=new Query(Order.KIND).setFilter(f);
    	PreparedQuery pq=ds.prepare(q);
    	for(Entity eDish: pq.asIterable()){
    		orders.add(new Order(eDish));
    	}
    	return orders;
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/deliveried_unpaid/{tid}")
    public List<Order> getUnpaidDeliveredOrder(@PathParam("tid") String tid){
    	long tableId=Long.parseLong(tid);
    	DatastoreService ds=this.getService();
    	List<Order> orders=new ArrayList<Order>();
    	Filter f1=new FilterPredicate("delivered" , FilterOperator.EQUAL, true);
    	Filter f2=new FilterPredicate("paid" , FilterOperator.EQUAL, false);
    	Filter f3=new FilterPredicate("table" , FilterOperator.EQUAL, tableId);
    	Filter f=CompositeFilterOperator.and(f1, f2, f3);
    	Query q=new Query(Order.KIND).setFilter(f);
    	PreparedQuery pq=ds.prepare(q);
    	for(Entity eDish: pq.asIterable()){
    		orders.add(new Order(eDish));
    	}
    	return orders;
    }
    
    
    /*
     * get order by table id;
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/get/{tid}")
    public List<Order> getOrder(@PathParam("tid") String tid) throws EntityNotFoundException{
    	this.refreshOrder();
 	   	ArrayList<Order> orders=new ArrayList<Order>();
    	if(!tid.matches("\\d+")) {
    		System.out.println("the id is not a number: "+tid);
    		return null;
    	}
    	System.out.println("start query order, id: "+tid);
    	
    	long id=Long.parseLong(tid);
    	
    	DatastoreService ds=this.getService();
    	Filter f=new FilterPredicate("table", FilterOperator.EQUAL, id);
    	Query q=new Query(Order.KIND).setFilter(f);
    	PreparedQuery pq=ds.prepare(q);
    	for(Entity eDish: pq.asIterable()){
    		orders.add(new Order(eDish));
    	}
    	return orders;
    }
    
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/post")
    public long putOrder(Order order){
    	if(order==null)return 0;
    	DatastoreService ds=this.getService();
    	order.timestamp=new Date();
    	order.handled=false;
    	order.paid=false;
    	    	
    	long price=0;
    	List<Long> dishIds=new ArrayList<Long>();
    	List<Key> keys=new ArrayList<Key>();
    	for(long i: order.dishes.keySet()){
    		dishIds.add(i);
    		keys.add(KeyFactory.createKey(Dish.KIND, i));
    	}
    	
    	Filter f=new FilterPredicate(Entity.KEY_RESERVED_PROPERTY, FilterOperator.IN,  keys);
    	Query q=new Query(Dish.KIND).setFilter(f);
    	q.addProjection(new PropertyProjection("price", Long.class));
    	PreparedQuery pa=ds.prepare(q);
    	Map<Long, Long> dishPrice=new HashMap<Long, Long>();
    	for(Entity e: pa.asIterable()){
    		long id=e.getKey().getId();
    		Object o= e.getProperty("price");
    		long p=(long)o;
    		dishPrice.put(id, p);
    	}
    	for(long id: dishIds){
    		price+=order.dishes.get(id)*dishPrice.get(id);
    	}
    	order.setPrice(price);
    	Key k=ds.put(order.getEntity());
    	return k.getId();
    	
    }
    
    
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/pay")
    public boolean pay(List<Order> orders) throws EntityNotFoundException{
    	
    	List<Entity> orderEntities=new ArrayList<Entity>();
    	DatastoreService ds=this.getService();
    	for(Order order: orders){
    		Key k=KeyFactory.createKey(Order.KIND, order.id);
    		Entity e=ds.get(k);
    		Order _order=new Order(e);
    		_order.paid=true;
    		orderEntities.add(_order.getEntity());
    	}
    	Transaction tx=ds.beginTransaction();
    	ds.put(orderEntities);
    	tx.commit();
    	return true;
    }
    
    
    
    
  
    
   
   

}
