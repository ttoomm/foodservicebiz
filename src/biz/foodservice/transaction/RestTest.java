package biz.foodservice.transaction;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import biz.foodservice.model.Order;
import biz.foodservice.model.CookingTask;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.Query;



@Path("/test")
public class RestTest   {

   
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/order/get")
    public Order getCatagories(){
    	Order order=new Order(99);
    	order.dishes.put(88L, 99L);
    	order.dishes.put(889L, 990L);
    	return order;
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/order/create")
    public Order createOrder(){
    	Order order=new Order(3);
    	order.dishes.put(22L, 2L);
    	order.dishes.put(23L, 1L);
    	order.timestamp=new Date();
    	DatastoreService ds=DatastoreServiceFactory.getDatastoreService();
    	Key k=ds.put(order.getEntity());
    	System.out.println("create an order with id: "+k.getId());
    	

    	return order;
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/order/update/{oid}")
    public Order updateOrder(@PathParam("oid") String oid){
    	Order order=new Order(5);
    	order.dishes.put(22L, 2L);
    	order.dishes.put(23L, 1L);
    	order.handled=true;
    	order.timestamp=new Date();
    	order.id=Long.parseLong(oid);
    	DatastoreService ds=DatastoreServiceFactory.getDatastoreService();
    	ds.put(order.getEntity());
    	return order;
    }
    
    
    
    @GET
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
    @Path("/task/finish/{tid}")
    public String finish(String tid) throws Exception{
    	CookingTaskOps ops=new CookingTaskOps();
	
 	   	//return ops.finish(tid);
    	return "";
    }
    
    @GET
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
    @Path("/task/pull")
    public String generate() throws Exception{
    	CookingTaskOps ops=new CookingTaskOps();
    	ops.generateTasks();
 	   	return ""+true;
    }
    
    
   
   

}
