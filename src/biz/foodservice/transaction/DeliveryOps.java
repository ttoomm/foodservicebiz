package biz.foodservice.transaction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import biz.foodservice.model.DeliveryTask;
import biz.foodservice.model.Dish;
import biz.foodservice.model.Order;
import biz.foodservice.model.CookingTask;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;

@Path("/transaction/delivery")
public class DeliveryOps {

	private DatastoreService ds;

	public DeliveryOps() {
		super();
	}

	private DatastoreService getService() {
		if (this.ds == null) {
			this.ds = DatastoreServiceFactory.getDatastoreService();
		}
		return this.ds;
	}

	/**
	 * cautions: only get invoked before a task is finished.
	 * 
	 * @throws Exception
	 */
	void generateDeliveryTasks() throws Exception {
		List<CookingTask> cookingtasks=CookingTaskOps.getTasks(this.getService(), CookingTask.FINISHED);
		
		for(CookingTask ct: cookingtasks){
			List<DeliveryTask> deliveries=new ArrayList<DeliveryTask>();	
			List<Entity> es=new ArrayList<Entity>();
			for(long orderId: ct.quantity.keySet()){
				Key k=KeyFactory.createKey(Order.KIND, orderId);
				Order order=new Order(ds.get(k));
				long tableId=order.table;
				DeliveryTask dt=new DeliveryTask();
				dt.cookingTaskId=ct.id;
				dt.dishId=ct.dishId;
				dt.status=DeliveryTask.status_ready;
				dt.orderDate=order.timestamp;
				dt.tableId=order.table;
				dt.orderId=order.id;
				deliveries.add(dt);
				es.add(dt.getEntity());
			}
			ct.status=CookingTask.SEND_TO_DELIVER;
			es.add(ct.getEntity());
			
			TransactionOptions options = TransactionOptions.Builder.withXG(true);
			Transaction tx=ds.beginTransaction(options);
			ds.put(es);
			tx.commit();
		}
		
		
	}
	
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
    @Path("/finish")
    public String finish(DeliveryTask task) throws Exception{
 	   	
    	
    	System.out.println("start query tasks, id: "+task.id);
    	
    	DatastoreService ds=this.getService();
    	Entity taske=ds.get(KeyFactory.createKey(DeliveryTask.KIND, task.id));
    	DeliveryTask taskFinished=new DeliveryTask(taske);
    	
    	if(taskFinished.status!=DeliveryTask.status_onDelivery){
    		throw new Exception("the task is not in the in_delivery state, task: "+taskFinished);
    	}
    	taskFinished.status=DeliveryTask.status_finished;
    	ds.put(taskFinished.getEntity());
    	this.generateDeliveryTasks();
    	
    	return ""+true;
    }
	
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
    @Path("/claim")
    public String claim(DeliveryTask task) {
    	
    	DatastoreService ds=this.getService();
    	Entity taske=null;
		try {
			taske = ds.get(KeyFactory.createKey(DeliveryTask.KIND, task.id));
		} catch (EntityNotFoundException e) {
			e.printStackTrace();
			return "claim failed, cause: "+e.getMessage();
		}
		DeliveryTask taskWaiting=new DeliveryTask(taske);
    	
    	if(taskWaiting.status!=CookingTask.WAITING){
    		return ""+(new Exception("the task is not in the waiting state, task: "+taskWaiting));
    	}
    	taskWaiting.status=CookingTask.PROCESSING;
    	taskWaiting.byWaiter=task.byWaiter;
    	Transaction tran=ds.beginTransaction();
    	ds.put(taskWaiting.getEntity());
    	tran.commit();
    	return ""+true;
    }
	
	@GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/unfinished")
	public List<DeliveryTask> getUnfinished() throws Exception {
				this.generateDeliveryTasks();
			
				return this.getTasks(this.getService(), DeliveryTask.status_onDelivery, DeliveryTask.status_ready);
	}
	

	List<DeliveryTask> getTasks(DatastoreService ds, long... statuses) throws Exception {

		List<DeliveryTask> tasks = new ArrayList<DeliveryTask>();
		
		ArrayList<Long> vs=new ArrayList<Long>();
		for(long status: statuses){
			vs.add(status);
		}
		
		Filter f = new FilterPredicate("status", FilterOperator.IN,vs				);
		Query q = new Query(DeliveryTask.KIND).setFilter(f);
		PreparedQuery pq = ds.prepare(q);
		for (Entity e : pq.asIterable()) {
			tasks.add(new DeliveryTask(e));
		}
		return tasks;
	}

	

}
