package biz.foodservice.transaction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import biz.foodservice.model.Dish;
import biz.foodservice.model.Order;
import biz.foodservice.model.CookingTask;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.Filter;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;
import com.google.appengine.api.datastore.Transaction;
import com.google.appengine.api.datastore.TransactionOptions;

@Path("/transaction/task")
public class CookingTaskOps {

	private DatastoreService ds;

	public CookingTaskOps() {
		super();
	}

	private DatastoreService getService() {
		if (this.ds == null) {
			this.ds = DatastoreServiceFactory.getDatastoreService();
		}
		return this.ds;
	}

	/**
	 * cautions: only get invoked before a task is finished.
	 * 
	 * @throws Exception
	 */
	void generateTasks() throws Exception {
		OrderOps oo = new OrderOps();
		DatastoreService ds = this.getService();
		List<Order> orders = oo.getUnhandledOrder();
		

		nextOrder: for (Order order : orders) {
			List<CookingTask> waitTasks = this.getWaitingTasks();
			List<CookingTask> newTasks = new ArrayList<CookingTask>();
			Set<CookingTask> modifiedTasks=new HashSet<CookingTask>();
			nextDish: for (Object o : order.dishes.keySet()) {
				System.out.println(o.getClass());
				long dishId=(long) o;
				/*
				 * try update existing waiting tasks;
				 */

				for (CookingTask task : waitTasks) {
					if (task.dishId == dishId) {
						task.addOrder(order);
						if(!modifiedTasks.contains(task) )modifiedTasks.add(task);
						continue nextDish;
					} 
				}

				/*
				 * try to update the new create tasks
				 */
				for (CookingTask task : newTasks) {
					if (task.dishId == dishId) {
						task.addOrder(order);
						continue nextDish;
					}
				}
				
				/*
				 * if no match, create a new task;
				 */
				CookingTask task = new CookingTask(dishId);
				task.addOrder(order);
				newTasks.add(task);
			}
			List<Entity> es=new ArrayList<Entity>();
			for(CookingTask task: newTasks){
				es.add(task.getEntity());
			}
			for(CookingTask task: modifiedTasks){
				es.add(task.getEntity());
			}
			order.handled=true;
			TransactionOptions options = TransactionOptions.Builder.withXG(true);
			Transaction tx=ds.beginTransaction(options);
			List<Key> ks=ds.put(es);
			for(Key k: ks){
				order.cookingTasks.add(k.getId());
			}
			ds.put(order.getEntity());
			tx.commit();
		}
		
		
	}
	
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
    @Path("/finish")
    public String finish(CookingTask task) throws Exception{
 	   	
    	
    	System.out.println("start query tasks, id: "+task.id);
    	
    	DatastoreService ds=this.getService();
    	Entity taske=ds.get(KeyFactory.createKey(CookingTask.KIND, task.id));
    	CookingTask taskFinished=new CookingTask(taske);
    	
    	if(taskFinished.status!=CookingTask.PROCESSING){
    		throw new Exception("the task is not in the processing state, task: "+taskFinished);
    	}
    	taskFinished.status=CookingTask.FINISHED;
    	ds.put(taskFinished.getEntity());
   	
    	this.generateTasks();
    	
    	return ""+true;
    }
	
	@POST
	@Produces(MediaType.TEXT_PLAIN)
	@Consumes(MediaType.APPLICATION_JSON)
    @Path("/claim")
    public String claim(CookingTask task) {
    	
    	DatastoreService ds=this.getService();
    	Entity taske=null;
		try {
			taske = ds.get(KeyFactory.createKey(CookingTask.KIND, task.id));
		} catch (EntityNotFoundException e) {
			e.printStackTrace();
			return "claim failed, cause: "+e.getMessage();
		}
    	CookingTask taskWaiting=new CookingTask(taske);
    	
    	if(taskWaiting.status!=CookingTask.WAITING){
    		return ""+(new Exception("the task is not in the waiting state, task: "+taskWaiting));
    	}
    	taskWaiting.status=CookingTask.PROCESSING;
    	taskWaiting.byChief=task.byChief;
    	Transaction tran=ds.beginTransaction();
    	ds.put(taskWaiting.getEntity());
    	tran.commit();
    	
    	return ""+true;
    	
    	
    }
	
	@GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/unfinished")
	public List<CookingTask> getUnfinished() throws Exception {
				this.generateTasks();
			
				return this.getTasks(this.getService(), CookingTask.WAITING, CookingTask.PROCESSING);
	}
	

	/*
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Path("/getWaiting")
	*/
	List<CookingTask> getWaitingTasks() throws Exception {

		return this.getTasks(this.getService(), CookingTask.WAITING);
	}
	
	
	
	static List<CookingTask> getTasks(DatastoreService ds, long... statuses) throws Exception {

		List<CookingTask> tasks = new ArrayList<CookingTask>();
		
		ArrayList<Long> vs=new ArrayList<Long>();
		for(long status: statuses){
			vs.add(status);
		}
		
		Filter f = new FilterPredicate("status", FilterOperator.IN,vs);
		Query q = new Query(CookingTask.KIND).setFilter(f);
		PreparedQuery pq = ds.prepare(q);
		for (Entity e : pq.asIterable()) {
			tasks.add(new CookingTask(e));
		}
		return tasks;
	}
	

	

}
