package biz.foodservice.model;

import java.io.Serializable;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.google.appengine.api.datastore.Blob;
import com.google.appengine.api.datastore.Entity;




public class Dish implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -3489358120638118850L;
	
	
	static public class Info {
		public long id=0;
		public String name="";
		public long price=0;

		public Info(){
		}
		public Info(long id, String name, long price) {
			this.id = id;
			this.name = name;
			this.price = price;
		}
		public Info(Entity e){
			this.id=e.getKey().getId();
			this.name=(String) e.getProperty("name");
			this.price=(long) e.getProperty("price");
			
		}
	}

	
	public Info info = new Info();
	public String desc="";
	public String iconUrl="";
	public String picUrl="";
	public Blob pic=new Blob(new byte[1]);
	public Blob icon=new Blob(new byte[1]);
	
	public Dish(){
	}
	

	
	public Dish(long id, String name, String desc,  String iconUrl, String picUrl, long price){
		this.info.id=id;
		this.info.name=name;
		this.desc=desc;
		this.info.price=price;
		this.desc=desc;
		this.iconUrl=iconUrl;
		this.picUrl=picUrl;
	}
	@JsonIgnore
	public void setPic(Blob blob){
		this.pic=blob;
	}
	@JsonIgnore
	public void setIcon(Blob blob){
		this.icon=blob;
	}
	
	
	public final static String KIND=Dish.class.getCanonicalName();
   
	@JsonIgnore
    public Entity getEntity(){
		Entity entity=new Entity(Dish.KIND, this.info.id);
		entity.setProperty("desc", this.desc);
		entity.setProperty("name", this.info.name);
		entity.setProperty("price", this.info.price);
		entity.setProperty("iconUrl", this.iconUrl);
		entity.setProperty("picUrl", this.picUrl);
		entity.setProperty("pic", this.pic);
		entity.setProperty("icon", this.icon);
		return entity;
	}
	
 public Dish(Entity e){

	this.info=new Dish.Info(e);
	this.desc=(String) e.getProperty("desc");
	this.iconUrl=(String) e.getProperty("iconUrl");
	this.picUrl=(String) e.getProperty("picUrl");
	this.pic=(Blob) e.getProperty("pic");
	this.icon=(Blob) e.getProperty("icon");
		
	}
	
	
}
