package biz.foodservice.model;

import java.util.Date;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnore;

import com.google.appengine.api.datastore.Entity;

public class DeliveryTask {
	public static final String KIND = DeliveryTask.class.getCanonicalName();
	public static long status_ready = -1, status_onDelivery = 0,
			status_finished = 1, status_HandleByPayment = 2;

	public long id = -1;
	public long byWaiter = -1;
	public long tableId = -1;

	public long dishId = -1;

	public Date orderDate = new Date(Long.MAX_VALUE);
	
	public long orderId=-1;

	public long status = -1;

	public long cookingTaskId = -1;

	@JsonIgnore
	public Entity getEntity() {
		Entity e = null;
		if (this.id > 0)
			e = new Entity(KIND, this.id);
		else {
			e = new Entity(KIND);
		}
		e.setProperty("byWaiter", this.byWaiter);
		e.setProperty("tableId", this.tableId);
		e.setProperty("dishId", this.dishId);
		e.setProperty("orderDate", this.orderDate);
		e.setProperty("status", this.status);
		e.setProperty("cookingTaskId", this.cookingTaskId);
		e.setProperty("orderId", this.orderId);
		return e;
	}
	
	public DeliveryTask(){
		
	}
	public DeliveryTask(Entity e){
		this();
		this.id=e.getKey().getId();
		this.byWaiter=(long) e.getProperty("byWaiter");
		this.tableId=(long) e.getProperty("tableId");
		this.dishId=(long)e.getProperty("dishId");
		this.orderDate=(Date) e.getProperty("orderDate");
		this.status=(long) e.getProperty("status");
		this.cookingTaskId=(long) e.getProperty("cookingTaskId");
		this.orderId=(long) e.getProperty("orderId");
	}

}
