package biz.foodservice.model;

import java.io.IOException;
import java.io.StringReader;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;

public class CommonTools {
	static public Map<Long, Long> fromString(String s) throws JsonParseException, JsonMappingException, IOException{
    	ObjectMapper om= new ObjectMapper();
    	Map<Long, Long> m=om.readValue(s, new TypeReference<Map<Long, Long>>(){});
    	return m;
    }
    static public String fromMap(Map<Long, Long> m) throws JsonGenerationException, JsonMappingException, IOException{
    	ObjectMapper om= new ObjectMapper();
    	return om.writeValueAsString(m);
    }
    
    static public List<Long> listFromString(String s) throws JsonParseException, JsonMappingException, IOException{
    	ObjectMapper om= new ObjectMapper();
    	List<Long> l=om.readValue(new StringReader(s), new TypeReference<List<Long>>(){});
    	return l;
    }
    
    static public String fromList(List<Long> l) throws JsonParseException, JsonMappingException, IOException{
    	ObjectMapper om= new ObjectMapper();
    	return om.writeValueAsString(l);
    	
    }
    
}
