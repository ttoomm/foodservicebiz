package biz.foodservice.model;

import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;

import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;


public class CookingTask implements Comparable<CookingTask>{
	public static final String KIND=CookingTask.class.getCanonicalName();
	static public final long FINISHED=1, WAITING=-1, PROCESSING=0, SEND_TO_DELIVER=2;
	
	public long status=WAITING; 
	/**Task ID*/
	public long id=-1; 
	public long dishId=-1;
	
	/**orderID--dish number*/
	public Map<Long, Long> quantity;
	public Date dateEarliest=new Date(Long.MAX_VALUE);	
	public long byChief=-1;
		
	
	
	public CookingTask(){
		this.quantity=new HashMap<Long, Long>();
	}
	public CookingTask(long dishId){
		this();
		this.dishId=dishId;
	}
	
	
	
	@JsonIgnore
	public void addOrder(Order order) throws Exception{
		if(this.status!=WAITING){
			throw new Exception("this task is finished, can't be adding more orders");
		}
		if(order.handled ){
			throw new Exception("order has been handled");
		} 
		if(!order.dishes.containsKey(this.dishId)){
			throw new Exception("the order doesn't contain the dish of this task");
		}
		
		this.quantity.put(order.id, order.dishes.get(this.dishId));
		
		
		if(order.timestamp.before(this.dateEarliest)){
			this.dateEarliest=order.timestamp;
		}
	}

	
	@JsonProperty("total")
	public long getTotal(){
		long s=0;
		for(long i: this.quantity.values()){
			s+=i;
		}
		return s;
	}
	
	@JsonProperty("total")
	public void setTotal(long total){
		
	}
	
	//put the task from waiting to executing
	void starting(long chief){
		this.byChief=chief;
		this.status=CookingTask.PROCESSING;
	};
		
	void finish(){
		this.status=CookingTask.FINISHED;
	};
	
	public CookingTask(Entity e){
		this.id=e.getKey().getId();
		
		this.dishId=(long) e.getProperty("dishId");
		String strJson=(String) e.getProperty("orders");
		try {
			this.quantity=CommonTools.fromString(strJson);
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		this.status=(long) e.getProperty("status");
		this.dateEarliest=(Date) e.getProperty("dateEarliest");
		this.byChief=(long) e.getProperty("byChief");
		
	}
	
	@JsonIgnore
	public Entity getEntity() {
		Entity e=null;
		if(id>0){
			
			e=new Entity(CookingTask.KIND, id);
		}else{
			e=new Entity(CookingTask.KIND);
		}
		
//		public long status=WAITING; 
		e.setProperty("status", this.status);
//		/**Task ID*/
//		public long id=-1; 
		
//		public long dish=-1;
		e.setProperty("dishId", this.dishId);
//		
//		/**orderID--dish number*/
//		public Map<Long, Integer> quantity;
		try {
			e.setProperty("orders", CommonTools.fromMap(this.quantity));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
//		public Date dateEarliest=new Date(Long.MAX_VALUE);
		e.setProperty("dateEarliest", this.dateEarliest);
//		public long byChief=-1;
		e.setProperty("byChief", this.byChief);
		return e;
	}
	
	@Override
	public int compareTo(CookingTask to) {
		if(this.status!=to.status){
			return this.status<to.status?1: -1;
		}
		if(this.dateEarliest!=to.dateEarliest) {
			return this.dateEarliest.before(dateEarliest)? 1: -1;
		}
		return this.dishId<this.dishId? 1:-1;
	}
	
}