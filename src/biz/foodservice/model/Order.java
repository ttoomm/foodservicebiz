package biz.foodservice.model;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.JsonMappingException;

import com.google.appengine.api.datastore.Entity;





public class Order implements Serializable{
	
	
	final public static String KIND=Order.class.getCanonicalName();
	/*
	 * data model
	 * */
	public boolean handled;
	
	public boolean delivered;
	public boolean paid;
	/**table ID*/
	public long table;
	/**orderID*/
	public long id=0;
	public Date timestamp;
	
	private long price=-1;
	
	@JsonProperty("price")
	public long getPrice(){
		return this.price;
	}
	
	@JsonIgnore
	public void setPrice(long price){
	
		this.price=price;
	}

	/** dishID - dish number*/
	public Map<Long, Long> dishes;
	
	
	public List<Long> cookingTasks;
	
	//public List<Long> tasks;
	
	//public long customer;
	
	public Order(long table) {
		this();
		this.table = table;
		
	}
	
	
	public Order(){
		this.cookingTasks=new ArrayList<Long>();
		this.dishes = new HashMap<Long, Long>();
		this.timestamp=new Date();
	}

	
	@Override
	public boolean equals(Object o) {
		if(o==null || o.getClass()!=this.getClass()){
			return false;
		}
		if(o==this){
			return true;
		}
		Order other=(Order) o;
		return other.id==this.id;
	}
	
	
	
	
	public Order(Entity e) {
		this();
		this.id= e.getKey().getId();
		
//		public boolean handled;
		this.handled=(boolean) e.getProperty("handled");

		this.paid=(boolean) e.getProperty("paid");
		this.delivered=(boolean) e.getProperty("delivered");

		this.table=(long) e.getProperty("table");	//	

		this.timestamp=(Date) e.getProperty("timestamp");
		this.price=(long) e.getProperty("price");
		
		Object o=e.getProperty("dishes");
		try {
			this.dishes=CommonTools.fromString((String)o);
		} catch (JsonParseException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (JsonMappingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		o=e.getProperty("cookingTasks");
		try {
			this.cookingTasks=CommonTools.listFromString((String) o);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
	

	
	@JsonIgnore
	public Entity getEntity(){
		Entity entity=null;
		if(this.id>0){
			entity=new Entity(Order.KIND, this.id);
		}else{
			entity=new Entity(Order.KIND );
		}
		
		entity.setProperty("handled", this.handled);
		entity.setProperty("paid", this.paid);
		entity.setProperty("delivered", this.delivered);
		entity.setProperty("table", this.table);
		entity.setProperty("timestamp", this.timestamp);
		entity.setProperty("price", this.getPrice());
		try {
			entity.setProperty("dishes", CommonTools.fromMap(this.dishes));
			entity.setProperty("cookingTasks", CommonTools.fromList(this.cookingTasks));
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return entity;
	}
	
}


