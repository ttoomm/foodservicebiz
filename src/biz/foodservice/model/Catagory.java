package biz.foodservice.model;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

import com.google.appengine.api.datastore.Entity;



public class Catagory implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 2551631080735967175L;
	public long id=0;
	public String name="";
	public String desc="";
	public String pic="";
	public List<Long> dishes;
	
	
	private Catagory(){
		this.dishes=new ArrayList<Long>();
	}
	public Catagory(long id, String name, String desc, String pic, List<Long> dishes){
		this();
		this.id=id;
		this.name=name;
		this.desc=desc;
		this.pic=pic;
		this.dishes=dishes;
	}
	
	public final static String KIND=Catagory.class.getCanonicalName();
	
	
	
	@JsonIgnore
	public Entity getEntity(){
		Entity entity=new Entity(KIND, this.id);
		entity.setProperty("name", this.name);
		entity.setProperty("desc", this.desc);
		entity.setProperty("dishes", this.dishes);
		return entity;
	}
	
	
	 public Catagory(Entity e) throws JsonMappingException, IOException{
		this();
		this.id= e.getKey().getId();
		this.desc=(String) e.getProperty("desc");
		this.name=(String) e.getProperty("name");
		this.pic=(String) e.getProperty("pic");
		
		ObjectMapper om= new ObjectMapper();
		
		//String str=(String) e.getProperty("dishes");
		this.dishes=(ArrayList<Long>) e.getProperty("dishes");
		
		//this.dishes=om.readValue(str, new TypeReference<List<Integer>>(){});
		
		
		
	}
	
	
	
}
