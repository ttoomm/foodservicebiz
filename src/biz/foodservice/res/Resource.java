package biz.foodservice.res;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.map.JsonMappingException;

import biz.foodservice.data.helper.DataPopulator;
import biz.foodservice.model.Catagory;
import biz.foodservice.model.Dish;

import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.EntityNotFoundException;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.PropertyProjection;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Query.FilterOperator;
import com.google.appengine.api.datastore.Query.FilterPredicate;



@Path("/resource")
public class Resource   {
	private DatastoreService ds;
	
    public Resource() {
        super();
    }
    private DatastoreService getService(){
    	if(this.ds==null){
    		this.ds=DatastoreServiceFactory.getDatastoreService();
    	}
    	return this.ds;
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/dish/{id}")
    public Dish getDish(@PathParam("id") String sid){
    
    	if(!sid.matches("\\d+")) {
    		System.out.println("the id is not a number: "+sid);
    		return null;
    	}

    	long id=Long.parseLong(sid);
    	DatastoreService ds=this.getService();
    	Query q=new Query(Dish.KIND);

    	Key k=KeyFactory.createKey(Dish.KIND, id);
    	
    	try {
			Entity e=ds.get(k);
			Dish dish=new Dish(e);
			System.out.println("download dish, id: "+dish.info.id);
			return dish;
		} catch (EntityNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
    	
    	
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/dish")
    public List<Dish.Info> getDishId(){
    	ArrayList<Dish.Info> dishes=new ArrayList<Dish.Info>();
    	DatastoreService ds=this.getService();
    	Query q=new Query(Dish.KIND);
    	q.addProjection(new PropertyProjection("name", String.class));
    	q.addProjection(new PropertyProjection("price", Long.class));
    	PreparedQuery pq=ds.prepare(q);
    	for(Entity eDish: pq.asIterable()){
    		Dish.Info info=new Dish.Info(eDish);
    		dishes.add(info);
    	}
    	System.out.println("download dishes");
    	return dishes;
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/catagory/{id}")
    public Catagory getCatagory(@PathParam("id") String sid) throws JsonMappingException, IOException{
       	if(!sid.matches("\\d+")) {
       		System.out.println("getCatagory: id is not an integer");
       		return null;
       	}
       	
    	long id=Long.parseLong(sid);
    	DatastoreService ds=this.getService();
    	Key k=KeyFactory.createKey(Catagory.KIND, id);
    	Query q=new Query(Catagory.KIND).setFilter(new FilterPredicate(Entity.KEY_RESERVED_PROPERTY, FilterOperator.EQUAL, k));
    	PreparedQuery pq=ds.prepare(q);
    	for(Entity e: pq.asIterable()){
    		Catagory c=new Catagory(e);
    		System.out.println("downloading of catagory, id: "+c.id);
    		return c;
    	}
    	return null;
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/catagory")
    public List<Catagory> getCatagories() throws JsonMappingException, IOException {
    	ArrayList<Catagory> cs=new ArrayList<Catagory>();
    	DatastoreService ds=this.getService();
    	Query q=new Query(Catagory.KIND);

    	PreparedQuery pq=ds.prepare(q);
    	for(Entity e: pq.asIterable()){
    		Catagory c=new Catagory(e);
    		cs.add(c);
    	}
    	System.out.println("download all catagories");
    	return cs;
    }
    
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/init/basic")
    public String init(){
    	DataPopulator populator=new DataPopulator();
		try {
			populator.populateDbWithURLData(true, false);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "basic information initiate failed, error: "+e.getMessage();
		}
		return "finish basic information initiate finished";
    }
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/init/blob")
    public String initBlob(){
    	DataPopulator populator=new DataPopulator();
		try {
			populator.populateDbWithURLData(false, true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return "Dish blob update failed, error: "+e.getMessage();
		}
		return "finish the update dish blob";
    }
    
   
   

}
