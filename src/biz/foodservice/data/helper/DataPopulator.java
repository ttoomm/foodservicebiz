package biz.foodservice.data.helper;

import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import biz.foodservice.model.Catagory;
import biz.foodservice.model.Dish;

import com.google.appengine.api.datastore.Blob;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;

public class DataPopulator implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -2197423233248194537L;

	String[][] getChickenDishes() {
		return new String[][] {
				// -------name
				new String[] { "Honey Sesame Chicken", "Orange Chicken",
						"SweetFire Chicken Breast",
						"String Bean Chicken Breast", "Kung Pao Chicken",
						"Mushroom Chicken", "Black Pepper Chicken",
						"Grilled Teriyaki Chicken", },
				// -------icon
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-chicken-honeysesame.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-chicken-orange.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-chicken-sweetfire.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-chicken-stringbean.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-chicken-kungpao.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-chicken-mushroom.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-chicken-blackpepper.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-chicken-grilledteriyaki.png", },
				// -------pic
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/wok-chicken-honeysesame.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok-chicken-orange.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok-chicken-sweetfire.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok-chicken-stringbean.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok-chicken-kungpao.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok-chicken-mushroom.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok-chicken-blackpepper.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-chicken-grilledteriyaki.jpg", },
				new String[] {
						"Honey Sesame Chicken Breast is made with thin, crispy strips of all-white meat chicken tossed with fresh-cut string beans, crisp yellow bell peppers in a sizzling hot wok with our new delicious honey sauce and topped off with sesame seeds.",
						"Orange Chicken is a dish inspired by the Hunan Province in South Central China. It is prepared with crispy boneless chicken bites, tossed in the wok with our secret sweet and spicy orange sauce. Panda's very own Executive Chef Andy brought this entree to life and it quickly became Panda's most beloved dish.",
						"SweetFire Chicken Breast features crispy, white-meat chicken bites tossed in the wok with red bell peppers, diced onions and juicy pineapple with a zesty, sweet chili sauce inspired by the flavors of Thailand.",
						"String Bean Chicken Breast is a Cantonese dish inspired by the Guangdong Province on the Southern Coast of China. It is prepared with marinated chicken breast, fresh-cut string beans and sliced onions, tossed in the wok with a mild ginger soy sauce.",
						"Kung Pao Chicken is a spicy stir-fry dish inspired by the Sichuan Province in Central China. It is prepared with marinated diced chicken, crunchy peanuts, diced red bell peppers and sliced zucchini, all tossed in the wok with fresh green onions.",
						"Mushroom Chicken is a Cantonese dish inspired by the Guangdong Province on the Southern Coast of China. It is prepared with marinated diced chicken, sliced zucchini and button mushrooms, all tossed in the wok with a mild ginger soy sauce.",
						"Black Pepper Chicken is a traditional dish inspired by the Hunan Province in South Central China. It is prepared with marinated diced chicken, chopped celery, sliced onions and fresh ground black pepper, tossed in the wok with a mild ginger soy sauce.",
						"Grilled Teriyaki Chicken features marinated chicken filets that are grilled to perfection then sliced to order and served with our sweet and savory sauce. Availability of Grilled Teriyaki Chicken may vary by location.  View our Nutritional & Allergen PDF for details."

				} };
	}

	String[][] getBeefDishes() {
		return new String[][] {
				// -------name
				new String[] { "Broccoli Beef", "Shanghai Angus Steak",
						"Beijing Beef", },
				// -------icon
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-beef-broccoli.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-beef-shanghaiangussteak.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-beef-beijing.png", },
				// -------pic
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok-beef-broccoli.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok-steak-shanghaiasian.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok-beef-beijing.jpg", },
				new String[] {
						"Broccoli Beef is a Cantonese dish inspired by the Guangdong Province on the Southern Coast of China. It is prepared with marinated sliced beef and fresh broccoli florets, tossed in the wok with a mild ginger soy sauce.",
						"Shanghai Angus Steak features thick-cut slices of marinated Angus Top Sirloin with crisp asparagus, freshly sliced mushrooms, all wok-tossed in our new zesty Asian steak sauce.",
						"Beijing Beef is a Sichuan-style dish inspired by Central China. It is prepared with crispy strips of marinated beef, bell peppers and sliced onions, tossed in the wok with a tangy sweet and spicy sauce."

				}

		};
	}

	String[][] getSeafoodDishes() {
		return new String[][] {
				// -------name
				new String[] { "Honey Walnut Shrimp", },
				// -------icon
				new String[] { "https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-shrimp-honeywalnut.png", },
				// -------pic
				new String[] { "https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok-shrimp-honeywalnut.jpg", },
				new String[] { "Honey Walnut Shrimp is a dish inspired by the shores of Shanghai. It features fresh tempura shrimp wok-tossed in a gourmet honey sauce and topped with glazed walnuts. It's a mouthwatering combination of sweet and crispy." }

		};
	}

	String[][] getRegionEntires() {
		return new String[][] {
				// -------name
				new String[] { "Eggplant Tofu", },
				// -------icon
				new String[] { "https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-other-eggplanttofu.png", },
				// -------pic
				new String[] { "https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/wok_other_eggplanttofu.jpg", },
				new String[] { "Eggplant Tofu is a dish inspired by the Sichuan Province in Central China prepared with lightly browned tofu, fresh eggplant and diced red bell peppers, tossed in the wok with a sweet and spicy sauce." }

		};
	}

	String[][] getSidesDishes() {
		return new String[][] {
				// -------name
				new String[] { "Chow Mein", "Fried Rice", "Mixed Veggies",
						"White Steamed Rice", "Brown Steamed Rice", },
				// -------icon
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-sides-chowmein.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-sides-friedrice.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-sides-mixedveggies.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-sides-steamedrice.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-sides-steamedbrownrice.png", },
				// -------pic
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-side-chowmein.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-sides-friedrice.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-sides-mixedveggies.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-sides-steamedrice.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-sides-steamedbrownrice.jpg", },
				new String[] {
						"Chow Mein is prepared with our noodles, tossed in the wok with shredded onions, crisp celery and fresh cabbage.",
						"Fried Rice is prepared with steamed white rice that is tossed in the wok with soy sauce, scrambled eggs, green peas, carrots and chopped green onions.",
						"Mixed Veggies is a stir-fry combination of fresh broccoli, zucchini, carrots, string beans and cabbage.",
						"White Steamed Rice is prepared by steaming white rice to perfection.",
						"Brown Steamed Rice is prepared by steaming brown rice to perfection." }

		};
	}

	String[][] getAppettizers() {
		return new String[][] {
				// -------name
				new String[] { "Chicken Egg Roll", "Veggie Spring Roll",
						"Hot and Sour Soup", "Chicken Potsticker",
						"Crispy Shrimp", "Cream Cheese Rangoon", },
				// -------icon
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-appetizers-ckeggrolls.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-appetizers-vgspringrolls.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-appetizers-hotsoursoup.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-appetizers-ckpotstickers.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-appetizer-crispyshrimp.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-appetizers-ccrangoons.png", },
				// -------pic
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-appetizers-ckeggrolls.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-appetizers-vgspringrolls.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/bowl-appetizers-hotsoursoup.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-appetizers-ckpotstickers.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-shrimp-crispy.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-appetizers-ccrangoons.jpg", },
				new String[] {
						"Chicken Egg Rolls are prepared with a mixture of cabbage, carrots, mushrooms, green onions and marinated chicken wrapped in a thin wonton wrapper and cooked to a golden brown.",
						"Veggie Spring Rolls are prepared with a mixture of cabbage, celery, carrots, green onions and Chinese noodles wrapped in a thin wonton wrapper and cooked to a golden brown.",
						"Hot & Sour Soup is a traditional Chinese soup prepared with vegetable stock, eggs, tofu and button mushrooms.",
						"Chicken Potstickers are prepared with a soft white dumpling filled with a combination of chicken, cabbage and onions that is pan seared on one side to a golden brown.",
						"Crispy Shrimp is prepared with marinated, butterflied shrimp that are cooked to a crispy golden brown.",
						"Cream Cheese Rangoons are prepared with a crisp wonton wrapper filled with a mixture of soft cream cheese and green onions, served with a side of sweet and sour sauce for dipping." }

		};
	}

	String[][] getDesserts() {
		return new String[][] {
				// -------name
				new String[] { "Fortune Cookies", "Chocolate Chunk Cookie", },
				// -------icon
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-desserts-fortunecookies.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-desserts-chocolatechunkcookies.png", },
				// -------pic
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-desserts-fortunecookies.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/plate-desserts-chocolatechunkcookies.jpg", },

		};
	}

	String[][] getDrink() {
		return new String[][] {
				// -------name
				new String[] { "Pepsi", "Diet Pepsi", "Dr Pepper",
						"Sierra Mist", "Mountain Dew", "Mug Root Beer",
						"Sobe Lean", "Lipton Brisk Raspberry",
						"Lipton No Calorie Brisk Peach", "Tropicana Lemonade",
						"Tropicana Pink Lemonade", "Tropicana Fruit Punch",
						"Aquafina", "Gatorade Lemon-Lime",
						"Izze Sparkling Blackberry", "Ocean Spray Apple Juice",
						"SoBe Green Tea", },
				// -------icon
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-pepsi.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-dietpepsi.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-drpepper.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-sierramist.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-mountaindew.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-mugrootbeer.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-sobecranberry.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-briskraspberryicedtea.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-briskpeachgreen.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-tropicanalemonade.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-tropicanapinklemonade.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-tropicanafruitpunch.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-aquafina.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-gatorade.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-izzejuice.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-oceansprayapplejuice.png",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/thumb-drinks-sobegreentea.png", },
				// -------pic
				new String[] {
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-pepsi.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-dietpepsi.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-drpepper.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-sierramist.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-mountaindew.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-mugrootbeer.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-sobecranberry.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-briskraspberryicedtea.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-briskpeachgreen.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-tropicanalemonade.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-tropicanapinklemonade.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/cup-drinks-tropicanafruitpunch.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/bottle-drinks-aquafina.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/bottle-drinks-gatorade.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/bottle-drinks-izzejuice.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/bottle-drinks-oceansprayapplejuice.jpg",
						"https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/bottle-drinks-sobegreentea.jpg", }, };

	}

	String[] getCatagories() {
		return new String[] { "Chicken Entrees", "Beef Entrees",
				"Seafood Entrees", "Regional Entrees", "Sides", "Appetizers",
				"Desserts", "Drinks" };

	}

	List<Dish> dishes = new ArrayList<Dish>();
	List<Catagory> cats = new ArrayList<Catagory>();

	void initData() {
		String[] strCats = this.getCatagories();
		String[][][] dishesData = new String[][][] { this.getChickenDishes(),
				this.getBeefDishes(), this.getSeafoodDishes(),
				this.getRegionEntires(), this.getSidesDishes(),
				this.getAppettizers(), this.getDesserts(), this.getDrink() };

		for (int i = 0; i < strCats.length; i++) {
			String nameCat = strCats[i];
			List<Long> dishIds = new ArrayList<Long>();

			for (int j = 0; j < dishesData[i][0].length; j++) {

				long idDish = (i + 1) * 10 + (j + 1);
				String name = dishesData[i][0][j];
				String icon = dishesData[i][1][j];
				String pic = dishesData[i][2][j];
				String desc = dishesData[i].length == 3 ? ""
						: dishesData[i][3][j];

				// assume the price is between $1 to $10
				int price = (int) (Math.random() * 1000);
				Dish dish = new Dish(idDish, name, desc, icon, pic, price);
				System.out.println("create dish " + name);

				dishIds.add(idDish);
				this.dishes.add(dish);
			}
			Catagory cat = new Catagory(i + 1, nameCat, "", "", dishIds);
			System.out.println("create catagory " + nameCat);
			this.cats.add(cat);

		}

	}

	ArrayList<Entity> es = new ArrayList<Entity>();

	void prepareEntities() {
		System.out.println("start initiate Entities");
		this.initData();
		System.out.println("fininitiate Entities");
		for (int i = 0; i < this.cats.size(); i++) {
			es.add(this.cats.get(i).getEntity());
		}
		for (int i = 0; i < this.dishes.size(); i++) {
			es.add(this.dishes.get(i).getEntity());
		}
	}

	private void deleteAll(String kind) {
		Query q = new Query(kind).setKeysOnly();
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		PreparedQuery pq = ds.prepare(q);
		List<Key> ks = new ArrayList<Key>();
		for (Entity e : pq.asIterable()) {
			ks.add(e.getKey());
		}
		ds.delete(ks);
	}

	public void updateDishBlob() throws IOException {
		System.out.println("start updating");
		Query q = new Query(Dish.KIND);
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		PreparedQuery pq = ds.prepare(q);
		for (Entity e : pq.asIterable()) {
			long id = e.getKey().getId();
			Dish dish = new Dish(e);
			if (dish.pic.getBytes().length < 100
					|| dish.icon.getBytes().length < 100) {
				dish.setIcon(this.getBlobFromUrl(new URL(dish.iconUrl)));
				dish.setPic(this.getBlobFromUrl(new URL(dish.picUrl)));
				ds.put(dish.getEntity());
				System.out.println("update the blob, dish id:"+dish.info.id);
			}
		}
	}

	private Blob getBlobFromUrl(URL url) throws IOException {
		InputStream is = url.openStream();
		byte[] cb = new byte[1024 * 1024];
		int total = 0;
		for (int i = is.read(cb, total, cb.length - total); i > 0; i = is.read(
				cb, total, cb.length - total)) {
			total += i;
		}
		is.close();
		return new Blob(Arrays.copyOf(cb, total));
	}

	public void populateDbWithURLData(boolean overwrite, boolean updateBlob)
			throws IOException {
		
		DatastoreService ds = DatastoreServiceFactory.getDatastoreService();
		if (overwrite) {
			this.initData();
			this.prepareEntities();
			this.deleteAll(Catagory.KIND);
			this.deleteAll(Dish.KIND);
			List<Entity> ees = new ArrayList<Entity>();
			for (int i = 0; i < es.size(); i++) {
				if (i % 24 == 1) {
					ds.put(ees);
					ees.clear();
				}
				ees.add(es.get(i));
			}
		}
		if (updateBlob) {
			this.updateDishBlob();
		}

	}

}
