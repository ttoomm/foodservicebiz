package biz.foodservice.data.helper;


import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.GeneralSecurityException;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map.Entry;
import java.util.Set;

import javax.xml.transform.TransformerException;

import biz.foodservice.model.Dish;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.api.client.googleapis.extensions.appengine.auth.oauth2.AppIdentityCredential;
import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.http.AbstractInputStreamContent;
import com.google.api.client.http.FileContent;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.drive.Drive;
import com.google.api.services.drive.DriveScopes;
import com.google.api.services.drive.model.File;
import com.google.api.services.drive.model.Permission;
import com.google.appengine.api.datastore.DatastoreService;
import com.google.appengine.api.datastore.DatastoreServiceFactory;
import com.google.appengine.api.datastore.Entity;
import com.google.appengine.api.datastore.PreparedQuery;
import com.google.appengine.api.datastore.Query;
import com.google.appengine.api.datastore.Text;


public class GDriveHelper {
	static final String APPLICATION_NAME="foodservicebiz";
	private Drive userAccount() throws GeneralSecurityException, IOException{
		
		final String clientId="869726777423-fmekq3t8jso9iv32uruauedmj88j6s6o.apps.googleusercontent.com";
		  //final String clientEmail="869726777423-fmekq3t8jso9iv32uruauedmj88j6s6o@developer.gserviceaccount.com";
		final String clientEmail="tomthinking@gmail.com";
		  String fp=this.getClass().getClassLoader().getResource("foodservicebiz-4d16bf3c2d03.p12").getFile();
		    System.out.println("filename: "+fp);
		    java.io.File key = new java.io.File(fp);
		
		JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
		HttpTransport httpTransport = GoogleNetHttpTransport.newTrustedTransport();
		GoogleCredential credential = new GoogleCredential.Builder()
		    .setTransport(httpTransport)
		    .setJsonFactory(JSON_FACTORY)
		    .setServiceAccountId(clientEmail)
		    .setServiceAccountPrivateKeyFromP12File(key)
		    .setServiceAccountScopes(Collections.singleton(DriveScopes.DRIVE))
		    
		    .build();
		Drive drive=new Drive.Builder(httpTransport, JSON_FACTORY, credential)
        .setApplicationName(APPLICATION_NAME)
        
        .build();
		return drive;
	}
	
	public String uploadToDrive() throws TransformerException, GeneralSecurityException, IOException{
		String s="";
		//Drive service=this.dauth();
		Drive service=this.userAccount();
		
		com.google.api.services.drive.Drive.Files fs=service.files();
		com.google.api.services.drive.Drive.Files.List lf=fs.list();
		Set<Entry<String, Object>> es=lf.entrySet();
		for(Entry<String, Object> e: es){
			s+=String.format("\n[ %s -> %s(%s)]\n", e.getKey(), e.getValue(), e.getValue().getClass());
		}
		
		//String msg= this.uploadFile(service, new URL("https://s3.amazonaws.com/PandaExpressWebsite/files/food/v3/bottle-drinks-sobegreentea.jpg"), "fdsfd");
		return s;
	}
	
	public void updateDishEntityWithGDrive() throws TransformerException, GeneralSecurityException, IOException{
		
	
		
		Query q=new Query(Dish.KIND);
		
		DatastoreService ds=DatastoreServiceFactory.getDatastoreService();
		PreparedQuery pq=ds.prepare(q);
		for(Entity e: pq.asIterable()){
			long id=e.getKey().getId();
			
			Text txtIcon=(Text) e.getProperty("icon");
			String strIcon=txtIcon.getValue();
			URL urlIcon=new URL(strIcon);
			Text txtPic=(Text) e.getProperty("pic");
			String strPic=txtPic.getValue();
			URL urlPic=new URL(strPic);
			
			Text txtDesc=(Text) e.getProperty("desc");
			String strDesc=txtDesc.getValue();
			
			String s=String.format("id: %d\nurl Icon: %s\nurl image: %s", id, urlIcon, urlPic);
			System.out.println(s);
			
			
			
			
		}
		
		
	}
	
	
	
	String uploadFile(Drive service, URL urlIcon, String strDesc) throws IOException{
		String s="";
		Permission myperm = new Permission();
		myperm.setType("anyone").setRole("reader");
		
		
		 File body = new File();
		    //body.setPermissions(newPermission);
		    body.setTitle("idddd");
		    body.setDescription(strDesc);
		    body.setMimeType("text/plain");
		    
		    String fp=this.getClass().getClassLoader().getResource("aa.txt").getFile();
		    System.out.println("filename: "+fp);
		    java.io.File fileContent = new java.io.File(fp);
		    
//		    FileReader fr=new FileReader(fileContent);
//		    char[] buf=new char[1000];
//		    int totall=fr.read(buf);
//		    //fr.close();
//		    
//		    System.out.println(new String(Arrays.copyOf(buf, totall)));
		    
		    
		    FileContent mediaContent = new FileContent("text/plain", fileContent);
		    
		    s+="\nstart uploading file\n";
		    File file = service.files().insert(body, mediaContent).execute();
		    s+="\nfinsih uploading file\n";
		    service.permissions().insert(file.getId(), myperm).execute();
		    s+="\nupdate the permission of file, id"+file.getId();
		    
		    //File f= service.files().insert(body, new MyMedia("image/png", urlIcon)).execute();
		    
		    String msg="download url: "+file.getDownloadUrl();
		    System.out.println(msg);
		    return s+=String.format("[%s]\n", msg);
	}
	 class MyMedia extends AbstractInputStreamContent{
			

			public MyMedia(String type, URL url) throws IOException {
				super(type);
				InputStream is = url.openStream();

				int bufSize=1024*1024*5;
				byte[] _buf = new byte[bufSize];
				int total=0;
				for (int i = 0; i >= 0 && total<bufSize ; i = is.read(_buf, total, bufSize-total)) {
					total+=i;
				}
				buf=Arrays.copyOf(_buf, total);
				is.close();
				System.out.println(total);
				
			}
			
			byte[] buf;

			@Override
			public long getLength() throws IOException {
				// TODO Auto-generated method stub
				return buf.length;
			}

			@Override
			public boolean retrySupported() {
				// TODO Auto-generated method stub
				return false;
			}

			@Override
			public InputStream getInputStream() throws IOException {
				ByteArrayInputStream is=new ByteArrayInputStream(this.buf);
				return is;
			}
			
		}
	Drive dauth() throws GeneralSecurityException, IOException{
		  final String APPLICATION_NAME="foodservicebiz";
		  /** Global instance of the HTTP transport. */
		    HttpTransport httpTransport;

		  /** Global instance of the JSON factory. */
		    final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
		 
		 
		  httpTransport = GoogleNetHttpTransport.newTrustedTransport();
		AppIdentityCredential credential =
			    new AppIdentityCredential(DriveScopes.all());
		Drive drive=new Drive.Builder(httpTransport, JSON_FACTORY, credential)
        .setApplicationName(APPLICATION_NAME)
        .build();
		return drive;
		
	}
	
	
	 Drive auth() throws TransformerException, GeneralSecurityException, IOException {
		 /** E-mail address of the service account. */
		  //private static final String SERVICE_ACCOUNT_EMAIL = "[[INSERT_SERVICE_ACCOUNT_EMAIL_HERE]]";
			  final String SERVICE_ACCOUNT_EMAIL = "tomthinking@gmail.com";
			  
			  final String clientId="869726777423-fmekq3t8jso9iv32uruauedmj88j6s6o.apps.googleusercontent.com";
			  final String clientEmail="869726777423-fmekq3t8jso9iv32uruauedmj88j6s6o@developer.gserviceaccount.com";

		  /** Bucket to list. */
		  //private static final String BUCKET_NAME = "[[INSERT_YOUR_BUCKET_NAME_HERE]]";

		  /** Global configuration of Google Cloud Storage OAuth 2.0 scope. */
		    final String STORAGE_SCOPE =
		      "https://www.googleapis.com/auth/devstorage.read_write";

		  /** Global instance of the HTTP transport. */
		    HttpTransport httpTransport;

		  /** Global instance of the JSON factory. */
		    final JsonFactory JSON_FACTORY = JacksonFactory.getDefaultInstance();
		 
		 
		  httpTransport = GoogleNetHttpTransport.newTrustedTransport();

	        // Build a service account credential.
		  	String k12=this.getClass().getClassLoader().getResource("foodservicebiz-4d16bf3c2d03.p12").getFile();
		  	
	        GoogleCredential credential = new GoogleCredential.Builder().setTransport(httpTransport)
	        /*    .setJsonFactory(JSON_FACTORY)
	            .setServiceAccountUser(clientEmail)
	            .setServiceAccountId(clientId)
	            .setServiceAccountScopes(Arrays.asList(DriveScopes.DRIVE, DriveScopes.DRIVE_FILE, DriveScopes.DRIVE_METADATA))
	            .setServiceAccountPrivateKeyFromP12File(new java.io.File(k12))
	            //.set
	            .build();
	            */
		    .setJsonFactory(JSON_FACTORY)
		    .setServiceAccountId("tomthinking@gmail.com")
		    .setServiceAccountPrivateKeyFromP12File(new java.io.File(k12))
		    .setServiceAccountScopes(Collections.singleton(DriveScopes.DRIVE_FILE))
		    .setServiceAccountUser("tomthinking@gmail.com")
		    .build();
	        System.out.println("setup the oauth2");
	        
	        
	        
	        Drive driver=new Drive.Builder(httpTransport, JSON_FACTORY, credential)
	        .setApplicationName(APPLICATION_NAME)
	        .build();
	        
	        return driver;

	        // Set up and execute a Google Cloud Storage request.
	        /*
	        HttpRequestFactory requestFactory = httpTransport.createRequestFactory(credential);
	        GenericUrl url = new GenericUrl(URI);
	        HttpRequest request = requestFactory.buildGetRequest(url);
	        HttpResponse response = request.execute();
	        String content = response.parseAsString();
	        */
	       //[END snippet]
/*
	         Instantiate transformer input.
	        StreamSource xmlInput = new StreamSource(new StringReader(content));
	        StreamResult xmlOutput = new StreamResult(new StringWriter());

	        // Configure transformer.
	        Transformer transformer = TransformerFactory.newInstance().newTransformer(); // An identity
	                                                                                     // transformer
	        transformer.setOutputProperty(OutputKeys.DOCTYPE_SYSTEM, "testing.dtd");
	        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
	        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
	        transformer.transform(xmlInput, xmlOutput);

	        // Pretty print the output XML.
	        System.out.println("\nBucket listing for " + BUCKET_NAME + ":\n");
	        System.out.println(xmlOutput.getWriter().toString());
	        System.exit(0);

	     */
		  }
}
