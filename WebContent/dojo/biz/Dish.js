define([
    "dojo/_base/declare", "dojo/_base/lang","dijit/_WidgetBase", "dojo/dom-construct", 
    "dijit/_TemplatedMixin", "dojo/domReady!"
    ], 
       function(declare, lang, _WidgetBase, domConst, _templateMixin){
	   return declare([_WidgetBase, _templateMixin], {
	       dish: null,
	       templateString: "<div style=\"float : left\"><div align=\"center\">${dish.name}</div>"
		   +"<div><img src=\"${dish.icon}\" width=\"150\" height=\"150\"></div>"
		   +"<div><span>${dish.price}</span>"
		   +"<span align=\"center\"><input type=\"CheckBox\"/></span></div>",
	       constructor: function(params, srcNodeRef){
		   this.inherited(arguments);
		   this.dish=params.dish;
	       },
	       postCreate: function(){
		   // every time the user clicks the button, increment the counter
		   this.connect(this.domNode, "onclick", "increment");
               },

               increment: function(){
		   console.debug("is selected: "+this.dish.name);
               }

	   });
});
