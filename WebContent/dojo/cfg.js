dojoConfig={
    asyn: true,
    
    packages:[{name: "dojo", location: "/dojo/dojo"},
	      {name: "dijit", location: "/dojo/dijit"},
	      {name: "dojox", location: "/dojo/dojox"},
	      {name:  "biz", location: "/dojo/biz"}]
    
}
